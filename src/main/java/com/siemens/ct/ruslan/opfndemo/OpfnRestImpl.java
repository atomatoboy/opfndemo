package com.siemens.ct.ruslan.opfndemo;

import feign.*;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;

import java.util.List;

public class OpfnRestImpl {
    public FeignRestClient client;

    public OpfnRestImpl() {
        client = Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .target(FeignRestClient.class, "http://jsonplaceholder.typicode.com");
    }

    public interface FeignRestClient {
        @RequestLine("GET /todos/{id}")
        Todo getOneTODO(@Param(("id")) Integer todoId);

        @RequestLine("GET /todos")
        List<Todo> getAll();

        @RequestLine("POST /posts")
        @Headers("Content-Type: application/json")
        @Body("%7B\"title\": \"{title}\", \"body\": \"{body}\", \"userId\": \"{userId}\"%7D")
        Response getResponseForCreatedPost(@Param("title") String title, @Param("body") String body, @Param("userId") Integer userId);
    }

    public class Todo {
        Integer userId;
        Integer id;
        String title;
        Boolean completed;

        @Override
        public String toString() {
            return "{ id=" + id + ", " +
                    "title='" + title + '\'' + ", " +
                    "completed=" + completed + " }\n";
        }
    }

    public class PostItem {
        Integer userId;
        Integer id;
        String title;
        String body;

        @Override
        public String toString() {
            return "{ userId=" + userId + ", " +
                    "id=" + id + ", " +
                    "title='" + title + '\'' + ", " +
                    "body='" + body + '\'' + " }\n";
        }
    }
}
