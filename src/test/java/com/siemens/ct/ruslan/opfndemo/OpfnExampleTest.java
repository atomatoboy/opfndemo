package com.siemens.ct.ruslan.opfndemo;

import com.google.gson.Gson;
import com.siemens.ct.ruslan.opfndemo.OpfnRestImpl.FeignRestClient;
import com.siemens.ct.ruslan.opfndemo.OpfnRestImpl.PostItem;
import com.siemens.ct.ruslan.opfndemo.OpfnRestImpl.Todo;
import feign.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class OpfnExampleTest {
    public static FeignRestClient opfnClient;

    @BeforeAll
    static void setup() {
        opfnClient = new OpfnRestImpl().client;
    }

    @Test
    public void givenFeignRestClient_shouldReturnOneTodo() {
        final Todo todoObj = opfnClient.getOneTODO(17);
        assertThat(todoObj.title)
                .contains("quo laboriosam deleniti aut qui");
    }

    @Test
    public void givenFeignRestClient_canReturnListOF200Entries() {
        List<Todo> todosList = opfnClient.getAll();
        assertThat(todosList).hasSize(200);
    }

    @Test
    public void givenFeignRestClient_shouldReturnSameTodo() {
        List<Todo> todosList = opfnClient.getAll();
        Todo td = todosList.get(16);
        assertThat(td.title)
                .contains("quo laboriosam deleniti aut qui");
    }

    @Test
    public void canCreatePostItem_shouldReturnWhatCreated() throws IOException {
        Response resp = opfnClient.getResponseForCreatedPost("Title string  with spaces", "Body string with spaces", 8);
        assertThat(resp.status()).isEqualTo(201);

        Gson gson = new Gson();
        PostItem postItem = gson.fromJson(resp.body().asReader(), PostItem.class);
        assertThat(postItem.id).isEqualTo(101);
    }

}